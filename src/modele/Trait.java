package modele;

import java.awt.Color;
import java.awt.Graphics;

/*
 * Classe Trait qui herite de FigureColoree
 * 
 * @author millarde1u
 * @author schwarz16u
 */
@SuppressWarnings("serial")
public class Trait extends FigureColoree {

	/*
	 * Constructeur de la classe Trait
	 * 
	 * @param c
	 * 		Couleur du trait
	 */
	public Trait(Color c) {
		super(c);
	}

	/*
	 * Methode nbPoints qui retourne le nombre de points qu'aura le trait
	 * 
	 * @return le nombre de points qu'aura le trait
	 */
	@Override
	public int nbPoints() {
		return 2;
	}

	/*
	 * Methode nbClics qui retourne le nombre de clics qu'il faut pour construire le trait
	 * 
	 * @return le nombre de clics qu'il faut pour construire le trait
	 */
	@Override
	public int nbClics() {
		return 2;
	}

	/*
	 * Methode modifierPoints qui modifie le trait conformement a un ensemble de points de saisie
	 * 
	 * @param tp_saisie
	 * 		Tableau contenant les nouveaux points de saisie
	 */
	@Override
	public void modifierPoints(Point[] tp_saisie) {
		tab_mem = tp_saisie;
	}

	/*
	 * Methode estDedans qui retourne true si le point dont les coordonnees qui sont passees en 
	 * parametre se trouve a l'interieur du trait
	 * 
	 * @return true si le point dont les coordonnees qui sont passees en parametre se trouve a l'interieur du trait
	 * 
	 * @param x
	 * 		Abscisse du point dont les coordonnees sont saisies
	 * @param y
	 * 		Ordonne du point dont les coordonnees sont saisies
	 */
	@Override
	public boolean estDedans(int x, int y) {
		if ((tab_mem[0].distance(new Point(x,y))+tab_mem[1].distance(new Point(x,y)) == tab_mem[0].distance(tab_mem[1]))){
			return true;
		}
		return false;
	}
	
	/*
	 * Methode affiche qui permet l'affichage d'un trait
	 * 
	 * @param g
	 * 		Environnement graphique du dessin
	 */
	@Override
	public void affiche(Graphics g){
		super.affiche(g);
		g.setColor(couleur);
		g.drawLine(tab_mem[0].rendreX(), tab_mem[0].rendreY(), tab_mem[1].rendreX()+1, tab_mem[1].rendreY()+1);
		g.drawLine(tab_mem[0].rendreX()+1, tab_mem[0].rendreY()+1, tab_mem[1].rendreX(), tab_mem[1].rendreY());
		g.drawLine(tab_mem[0].rendreX()+1, tab_mem[0].rendreY(), tab_mem[1].rendreX()+1, tab_mem[1].rendreY());
		g.drawLine(tab_mem[0].rendreX(), tab_mem[0].rendreY()+1, tab_mem[1].rendreX(), tab_mem[1].rendreY()+1);
		g.drawLine(tab_mem[0].rendreX()+1, tab_mem[0].rendreY(), tab_mem[1].rendreX(), tab_mem[1].rendreY()+1);

	}
	
	/*
	 * Methode clone qui retourne un trait ayant les memes caracteristiques 
	 * que celui que l'on vient de creer
	 * 
	 * @return un trait ayant les memes caracteristiques que celui que l'on vient de creer
	 */
	@Override
	public FigureColoree clone(){
		return new Trait(this.couleur);
	}

}