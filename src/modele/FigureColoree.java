package modele;

import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;

/*
 * Classe abstraite FigureColoree qui represente le sommet de la hierarchie d'heritage de n'importe quelle figure 
 * geometrique visuable a l'ecran et manipulable a l'aide de la souris. Cette classe s'inspire du fonctionnement des 
 * logiciels de dessin vectoriel, ou une figure est :
 * 		- definie a l'aide de points de saisie (par exemple les deux etremites d'une diagonale d'un rectangle)
 * 		- memorisee comme un ensemble de points qui permettent de la manipuler (par exemple les 4 sommets d'un rectangle  
 * 
 * @author millarde1u
 * @author schwarz16u
 */
@SuppressWarnings("serial")
public abstract class FigureColoree implements Serializable{
	
	/*
	 * Constate definissant la taille des carres de selection
	 */
	public static final int TAILLE_CARRE_SELECTION = 5;
	/*
	 * Booleen qui indique si la figure est selectionnee (son affichage est alors different)
	 */
	protected boolean selected;
	/*
	 * Couleur du remplissage de la figure coloree
	 */
	protected Color couleur;
	/*
	 * Tableau des points de memorisation de la figure
	 */
	protected Point[] tab_mem;
	
	/*
	 * Constructeur de la classe FigureColoree
	 * 
	 * @param c
	 * 		Couleur du remplissage de la figure coloree
	 */
	public FigureColoree(Color c) {
		this.selected = false;
		this.couleur = c;
		tab_mem = new Point[this.nbPoints()];
	}
	
	/*
	 * Methode abstraite nbPoints qui retourne le nombre de points de la figure
	 * 
	 * @return le nombre de points de la figure
	 */
	public abstract int nbPoints();
	
	/*
	 * Methode abstraite nbClics qui retourne le nombre de clics qu'il faut pour construire la figure
	 * 
	 * @return le nombre de clics qu'il faut pour construire la figure
	 */
	public abstract int nbClics();
	
	/*
	 * Methode abstraite modifierPoints
	 * 
	 * @param tp
	 * 		Tableau des points de memorisation de la figure
	 */
	public abstract void modifierPoints(Point[] tp);
	
	/*
	 * Methode affiche qui permet l'affichage de la figure. Si la figure est selectionnee, des petits carres sont dessines 
	 * autour des points de memorisation
	 *  
	 * @param g
	 * 		Environnement graphique du dessin
	 */
	public void affiche(Graphics g) {
		if (selected){
			if (tab_mem != null){
				for (int i=0; i<tab_mem.length;i++){
					if(tab_mem[i] != null){
						g.setColor(Color.black);
						g.fillRect(tab_mem[i].rendreX()-(FigureColoree.TAILLE_CARRE_SELECTION/2), tab_mem[i].rendreY()-(FigureColoree.TAILLE_CARRE_SELECTION/2), FigureColoree.TAILLE_CARRE_SELECTION, FigureColoree.TAILLE_CARRE_SELECTION);
						g.setColor(couleur);
					}
				}
			}
		}
	}
	
	/*
	 * Methode transformation qui permet d'effectuer une transformation des coordonees des points de memorisation de la figure
	 * 
	 * @param dx
	 * 		Deplacement sur l'axe des abscisses
	 * @param dy
	 * 		Deplacement sur l'axe des ordonnees
	 * @param ind
	 * 		Indicedu point a modifier dans le tableau tab_mem 
	 */
	public void transformation(int dx, int dy, int ind){
		Point p=new Point(dx, dy);
		tab_mem[ind] = p;
		this.modifierPoints(tab_mem);
	}
	
	/*
	 * Methode abstraite estDedans qui retourne true si le point dont les coordonnees sont passees en parametre se 
	 * trouve a l'interieur de la figure
	 * 
	 * @return true si le point dont les coordonnees sont passees en parametre se trouve a l'interieur de la figure
	 * 
	 * @param x
	 * 		Abscisse du point
	 * @param y
	 * 		Ordonnee du point
	 */
	public abstract boolean estDedans(int x, int y);
	
	/*
	 * Methode carreDeSelection qui retourne l'indice dans tab_mem du point se trouvant pres d'un carre de selection
	 * 
	 * @return l'indice dans tab_mem du point se trouvant pres d'un carre de selection
	 * 
	 * @param x
	 * 		Abscisse d'un clic de souris
	 * @param y
	 * 		Ordonne d'un clic de souris
	 */
	public int carreDeSelection(int x, int y){
		int ind = -1;
		for(int i=0;i<tab_mem.length;i++){
	        if (tab_mem[i].etreProche(x,y,TAILLE_CARRE_SELECTION) == true){
	        	ind = i;
	        }
		}
		return ind;
	}
	
	/*
	 * Methode translation qui permet d'effectuer une transformation des coordonees des points de memorisation de la figure
	 * 
	 * @param dx
	 * 		Deplacement sur l'axe des abscisses
	 * @param dy
	 * 		Deplacement sur l'axe des ordonnees
	 */
	public void translation(int dx,int dy){
		for (int i=0; i<tab_mem.length;i++){
			tab_mem[i].translation(dx,dy);
		}
		this.modifierPoints(tab_mem);
	}
	
	/*
	 * Methode selectionne qui permet de selectionner la figure
	 */
	public void selectionne() {
		this.selected = true;
	}
	
	/*
	 * Methode deSelectionne qui peremet de deselectionner la figure
	 */
	public void deSelectionne() {
		this.selected = false;
	}
	
	/*
	 * Methode changeCouleur qui permet de changer la couleur de la figure
	 * 
	 * @param coul
	 * 		Nouvelle couleur que l'on veut attribuer a la figure
	 */
	public void changeCouleur(Color coul) {
		if (coul != null) {
			this.couleur = coul;
		}
	}
	
	/*
	 * Methode abstraite clone qui retourne une figure ayant les memes caracteristiques 
	 * que celle que l'on vient de creer
	 * 
	 * @return une figure ayant les memes caracteristiques que celle que l'on vient de creer
	 * 
	 */
	public abstract FigureColoree clone();

	/*
	 * Methode getColor qui retourne la couleur de la figure
	 * 
	 * @return la couleur de la figure
	 */
	public Color getColor(){
		return this.couleur;
	}
}