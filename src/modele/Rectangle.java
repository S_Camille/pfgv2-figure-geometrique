package modele;

import java.awt.Color;
import java.awt.Graphics;

/*
 * Classe Rectangle qui herite de Quadrilatere
 * 
 * @author millarde1u
 * @author schwarz16u
 */
@SuppressWarnings("serial")
public class Rectangle extends Quadrilatere {

	/*
	 * Constructeur de la classe Rectangle
	 * 
	 * @param c
	 * 		Couleur du rectangle
	 */
	public Rectangle(Color c) {
		super(c);
	}

	/*
	 * Methode nbClics qui retourne le nombre de clics qu'il faut pour construire le rectangle
	 * 
	 * @return le nombre de clics qu'il faut pour construire le rectangle
	 */
	@Override
	public int nbClics() {
		return 2;
	}

	/*
	 * Methode modifierPoints qui modifie le rectangle conformement a un ensemble de points de saisie
	 * 
	 * @param tp_saisie
	 * 		Tableau contenant les nouveaux points de saisie
	 */
	@Override
	public void modifierPoints(Point[] tp_saisie){
		if (tp_saisie.length == 2) {
			Point[] p={new Point(tp_saisie[0].rendreX(), tp_saisie[0].rendreY()),new Point(tp_saisie[0].rendreX(), tp_saisie[1].rendreY()),new Point(tp_saisie[1].rendreX(), tp_saisie[1].rendreY()),new Point(tp_saisie[1].rendreX(), tp_saisie[0].rendreY())};
			super.modifierPoints(p);
		}
		else {
			Point[] p = {new Point(tp_saisie[0].rendreX(), tp_saisie[0].rendreY()), new Point(tp_saisie[0].rendreX(), tp_saisie[2].rendreY()), new Point(tp_saisie[2].rendreX(), tp_saisie[2].rendreY()), new Point(tp_saisie[2].rendreX(), tp_saisie[0].rendreY())};
			if (p[0].rendreX() > p[2].rendreX()) {
				Point p0 = new Point(p[2].rendreX(), p[0].rendreY());
				Point p2 = new Point(p[0].rendreX(), p[2].rendreY());
				p[0] = p0;
				p[2] = p2;
			}
			if (p[0].rendreY() > p[2].rendreY()) {
				Point p0 = new Point(p[0].rendreX(), p[2].rendreY());
				Point p2 = new Point(p[2].rendreX(), p[0].rendreY());
				p[0] = p0;
				p[2] = p2;
			}
			super.modifierPoints(p);
		}
	}
	
	/*
	 * Methode affiche qui permet l'affichage d'un rectangle
	 * 
	 * @param g
	 * 		Environnement graphique du dessin
	 */
	@Override
	public void affiche(Graphics g) {
		if (selected){
			if (tab_mem != null){
				for (int i=0; i<tab_mem.length;i+=2){
					if(tab_mem[i] != null){
						g.setColor(Color.black);
						g.fillRect(tab_mem[i].rendreX()-(FigureColoree.TAILLE_CARRE_SELECTION/2), tab_mem[i].rendreY()-(FigureColoree.TAILLE_CARRE_SELECTION/2), FigureColoree.TAILLE_CARRE_SELECTION, FigureColoree.TAILLE_CARRE_SELECTION);
						g.setColor(couleur);
					}
				}
			}
		}
		if (p!=null){
			g.setColor(couleur);
			g.fillPolygon(p);
		}
	}
	
	/*
	 * Methode clone qui retourne un rectangle ayant les memes caracteristiques 
	 * que celui que l'on vient de creer
	 * 
	 * @return un rectangle ayant les memes caracteristiques que celui que l'on vient de creer
	 */
	@Override
	public FigureColoree clone() {
		return new Rectangle(this.couleur);
	}
	
}