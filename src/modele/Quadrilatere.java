package modele;

import java.awt.Color;

/*
 * Classe Quadrilatere qui herite de Polygone
 * 
 * @author millare1u
 * @author schwarz16u
 */
@SuppressWarnings("serial")
public class Quadrilatere extends Polygone {

	/*
	 * Constructeur de la classe Quadrilatere
	 * 
	 * @param c
	 * 		Couleur du qaudrilatere
	 */
	public Quadrilatere(Color c) {
		super(c);
	}

	/*
	 * Methode nbPoints qui retourne le nombre de points qu'aura le quadrilatere
	 * 
	 * @return le nombre de points qu'aura le quadrilatere
	 */
	public int nbPoints() {
		return 4;
	}

	/*
	 * Methode clone qui retourne un quadrilatere ayant les memes caracteristiques 
	 * que celui que l'on vient de creer
	 * 
	 * @return un quadrilatere ayant les memes caracteristiques que celui que l'on vient de creer
	 * 
	 */
	@Override
	public FigureColoree clone() {
		return new Quadrilatere(this.couleur);
	}

}