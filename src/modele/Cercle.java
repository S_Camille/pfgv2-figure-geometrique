package modele;

import java.awt.*;

/*
 * Classe Cercle qui herite de FigureColoree
 */
@SuppressWarnings("serial")
public class Cercle extends FigureColoree {

    /*
     * Attribut definissant le rayon du cercle cree
     */
    private double rayon;

    /*
     * Constructeur de la classe Cercle
     * @param c
     * 		Couleur que l'on souhaite donner a la figure
     */
    public Cercle(Color c){
        super(c);
    }

    /*
     * Methode nbPoints qui retourne le nombre de points qu'aura le cercle
	 * 
	 * @return le nombre de points qu'aura le cercle
	 */
    @Override
    public int nbPoints() {
        return 2;
    }

    /*
	 * Methode nbClics qui retourne le nombre de clics qu'il faut pour construire le cercle
	 * 
	 * @return le nombre de clics qu'il faut pour construire le cercle
	 */
    @Override
    public int nbClics() {
        return 2;
    }


    /*
     * Methode permttant d'afficher un cercle a l'ecran
     *
     * @param g
     *          Environnement graphique du dessin
     */
    @Override
    public void affiche(Graphics g){
        rayon = tab_mem[0].distance(tab_mem[1]);
        g.setColor(couleur);
        g.fillOval(tab_mem[0].rendreX()-((int)rayon/2),tab_mem[0].rendreY()-((int)rayon/2), (int)rayon, (int)rayon);
    }


    /*
	 * Methode modifierPoints qui modifie le trait conformement a un ensemble de points de saisie
	 * 
	 * @param tp_saisie
	 * 		Tableau contenant les nouveaux points de saisie
	 */
    @Override
    public void modifierPoints(Point[] tp_saisie) {
        tab_mem = tp_saisie;
    }
    
    /*
	 * Methode estDedans qui retourne true si le point dont les coordonnees qui sont passees en 
	 * parametre se trouve a l'interieur du cercle
	 * 
	 * @return true si le point dont les coordonnees qui sont passees en parametre se trouve a l'interieur du cercle
	 * 
	 * @param x
	 * 		Abscisse du point dont les coordonnees sont saisies
	 * @param y
	 * 		Ordonne du point dont les coordonnees sont saisies
	 */
    @Override
    public boolean estDedans(int x, int y) {
        Point p=new Point(x,y);
        if (tab_mem[0].distance(p) <= rayon){
            return true;
        }
        else{
            return false;
        }
    }

    /*
	 * Methode clone qui retourne un cercle ayant les memes caracteristiques 
	 * que celui que l'on vient de creer
	 * 
	 * @return un cercle ayant les memes caracteristiques que celui que l'on vient de creer
	 */
    @Override
    public FigureColoree clone() {
        return new Cercle(couleur);
    }
}
