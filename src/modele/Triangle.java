package modele;

import java.awt.Color;

/*
 * Classe Triangle qui herite de Polygone
 * 
 * @author millarde1u
 * @author schwarz16u
 */
@SuppressWarnings("serial")
public class Triangle extends Polygone {

	/*
	 * Constructeur de la classe Triangle
	 * 
	 * @param c
	 * 		Couelur du triangle
	 */
	public Triangle(Color c) {
		super(c);
	}
	
	/*
	 * Methode nbClics qui retourne le nombre de clics qu'il faut pour construire le triangle
	 * 
	 * @return le nombre de clics qu'il faut pour construire le triangle
	 */
	@Override
	public int nbClics(){
		return 3;
	}
	
	/*
	 * Methode nbPoints qui retourne le nombre de points qu'aura le triangle
	 * 
	 * @return le nombre de points qu'aura le triangle
	 */
	public int nbPoints() {
		return 3;
	}
	
	/*
	 * Methode clone qui retourne un triangle ayant les memes caracteristiques 
	 * que celui que l'on vient de creer
	 * 
	 * @return un triangle ayant les memes caracteristiques que celui que l'on vient de creer
	 * 
	 */
	@Override
	public FigureColoree clone(){
		return new Triangle(this.couleur);
	}
	
}