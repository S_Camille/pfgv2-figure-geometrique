package modele;

import java.awt.Color;

/*
 * Classe Carre qui herite de Rectangle
 * 
 * @author millarde1u
 * @author schwarz16u
 */
@SuppressWarnings("serial")
public class Carre extends Rectangle {

	/*
	 * Constructeur de la classe Carre
	 * 
	 * @param c
	 * 		Couleur du carre
	 */
	public Carre(Color c) {
		super(c);
	}

	/*
	 * Methode modifierPoints qui modifie le carre conformement a un ensemble de points de saisie
	 * 
	 * @param tp_saisie
	 * 		Tableau contenant les nouveaux points de saisie
	 */
	@Override
	public void modifierPoints(Point[] tp_saisie) {
		if(tp_saisie.length == 2) {
			Point[] p = tp_saisie;
			int x, y;
			if (tp_saisie[0].rendreX() - tp_saisie[1].rendreX() > 0) {
				x = tp_saisie[0].rendreX() - tp_saisie[1].rendreX();
			} else {
				x = tp_saisie[1].rendreX() - tp_saisie[0].rendreX();
			}
			if (tp_saisie[0].rendreY() - tp_saisie[1].rendreY() > 0) {
				y = tp_saisie[0].rendreY() - tp_saisie[1].rendreY();
			} else {
				y = tp_saisie[1].rendreY() - tp_saisie[0].rendreY();
			}
			if (x > y) {
				x = y;
			}
			if (tp_saisie[0].rendreX() > tp_saisie[1].rendreX()) {
				if (tp_saisie[0].rendreY() > tp_saisie[1].rendreY()) {
					p[1] = new Point(tp_saisie[0].rendreX() - x, tp_saisie[0].rendreY() - x);
				} else {
					p[1] = new Point(tp_saisie[0].rendreX() - x, tp_saisie[0].rendreY() + x);
				}
			} else {
				if (tp_saisie[0].rendreY() > tp_saisie[1].rendreY()) {
					p[1] = new Point(tp_saisie[0].rendreX() + x, tp_saisie[0].rendreY() - x);
				} else {
					p[1] = new Point(tp_saisie[0].rendreX() + x, tp_saisie[0].rendreY() + x);
				}
			}
			super.modifierPoints(p);
		}
		else{
			Point[] p = new Point[2];
			p[0] = tp_saisie[0];
			int x, y;
			if (tp_saisie[0].rendreX() - tp_saisie[2].rendreX() > 0) {
				x = tp_saisie[0].rendreX() - tp_saisie[2].rendreX();
			} else {
				x = tp_saisie[2].rendreX() - tp_saisie[0].rendreX();
			}
			if (tp_saisie[0].rendreY() - tp_saisie[2].rendreY() > 0) {
				y = tp_saisie[0].rendreY() - tp_saisie[2].rendreY();
			} else {
				y = tp_saisie[2].rendreY() - tp_saisie[0].rendreY();
			}
			if (x > y) {
				x = y;
			}
			if (tp_saisie[0].rendreX() > tp_saisie[2].rendreX()) {
				if (tp_saisie[0].rendreY() > tp_saisie[2].rendreY()) {
					p[1] = new Point(tp_saisie[0].rendreX() - x, tp_saisie[0].rendreY() - x);
				} else {
					p[1] = new Point(tp_saisie[0].rendreX() - x, tp_saisie[0].rendreY() + x);
				}
			} else {
				if (tp_saisie[0].rendreY() > tp_saisie[2].rendreY()) {
					p[1] = new Point(tp_saisie[0].rendreX() + x, tp_saisie[0].rendreY() - x);
				} else {
					p[1] = new Point(tp_saisie[0].rendreX() + x, tp_saisie[0].rendreY() + x);
				}
			}
			super.modifierPoints(p);
		}

	}


	/*
	 * Methode clone qui retourne un carre ayant les memes caracteristiques 
	 * que celui que l'on vient de creer
	 * 
	 * @return un carre ayant les memes caracteristiques que celui que l'on vient de creer
	 */
	@Override
	public FigureColoree clone() {
		return new Carre(this.couleur);
	}
	
}