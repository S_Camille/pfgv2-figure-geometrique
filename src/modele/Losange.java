package modele;

import java.awt.Color;

/*
 * Classe Losange qui herite de Quadrilatere
 * 
 * @author millarde1u
 * @author schwarz16u
 */
@SuppressWarnings("serial")
public class Losange extends Quadrilatere {

	/*
	 * Constructeur de la classe Losange
	 * 
	 * @param c
	 * 		Couleur du losange
	 */
	public Losange(Color c) {
		super(c);
	}

	/*
	 * Methode nbClics qui retourne le nombre de clics qu'il faut pour construire le losange
	 * 
	 * @return le nombre de clics qu'il faut pour construire le losange
	 */
	@Override
	public int nbClics() {
		return 2;
	}

	/*
	 * Methode modifierPoints qui modifie le losange conformement a un ensemble de points de saisie
	 * 
	 * @param tp_saisie
	 * 		Tableau contenant les nouveaux points de saisie
	 */
	@Override
	public void modifierPoints(Point[] tp_saisie) {
		int x  = tp_saisie[0].rendreX()+((tp_saisie[1].rendreX() - tp_saisie[0].rendreX())*2), y= tp_saisie[1].rendreY()-((tp_saisie[1].rendreY() - tp_saisie[0].rendreY())*2);
		Point[] p={new Point(tp_saisie[0]), new Point(tp_saisie[1]), new Point(x, tp_saisie[0].rendreY()), new Point(tp_saisie[1].rendreX(),y)};
		super.modifierPoints(p);
	}
	
	/*
	 * Methode clone qui retourne un losange ayant les memes caracteristiques 
	 * que celui que l'on vient de creer
	 * 
	 * @return un losange ayant les memes caracteristiques que celui que l'on vient de creer
	 */
	@Override
	public FigureColoree clone() {
		return new Losange(this.couleur);
	}
	
}