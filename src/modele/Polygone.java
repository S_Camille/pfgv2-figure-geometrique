package modele;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

/*
 * Classe abstraite Polygone, qui herite de FigureColoree, qui est la super classe de tous les polygones
 * 
 * @author millarde1u
 * @author schwarz16u
 */
@SuppressWarnings("serial")
public abstract class Polygone extends FigureColoree {
	
	/*
	 * Polygone de l'API Java (classe Polygon)
	 */
	protected Polygon p;
	
	/*
	 * Constructeur de la classe Polygone
	 * 
	 * @param c
	 * 		Couleur du polygone
	 */
	public Polygone(Color c) {
		super(c);
		this.p = null;
	}
	
	/*
	 * Methode affiche qui permet l'affichage d'un polygone
	 * 
	 * @param g
	 * 		Environnement graphique du dessin
	 */
	@Override
	public void affiche(Graphics g) {
		if (p!=null){
			g.setColor(couleur);
			g.fillPolygon(p);
		}
		super.affiche(g);
	}
	
	/*
	 * Methode nbClics qui retourne le nombre de clics qu'il faut pour construire le polygone
	 * 
	 * @return le nombre de clics qu'il faut pour construire le polygone
	 */
	public int nbClics(){
		return 4;
	}
	
	/*
	 * Methode modifierPoints qui modifie le polygone conformement a un ensemble de points de saisie
	 * 
	 * @param tp_saisie
	 * 		Tableau contenant les nouveaux points de saisie
	 */
	public void modifierPoints(Point[] tp_saisie) {
		tab_mem = tp_saisie;
		if (tab_mem[this.nbPoints()-1] != null){
			int[] xPoint=new int[tab_mem.length];
			int[] yPoint=new int[tab_mem.length];
			for(int i=0; i<tab_mem.length;i++){
				xPoint[i] = tab_mem[i].rendreX();
				yPoint[i] = tab_mem[i].rendreY();
			}
			p = new Polygon(xPoint,yPoint,tab_mem.length);
		}
	}
	
	/*
	 * Methode estDedans qui retourne true si le point dont les coordonnees qui sont passees en 
	 * parametre se trouve a l'interieur du polygone
	 * 
	 * @return true si le point dont les coordonnees qui sont passees en 
	 * parametre se trouve a l'interieur du polygone
	 * 
	 * @param x
	 * 		Abscisse du point dont les coordonnees sont saisies
	 * @param y
	 * 		Ordonne du point dont les coordonnees sont saisies
	 */
	public boolean estDedans(int x, int y){
		boolean t=false;
		if (p.contains(x,y)){
			t=true;
		}
		if (!t){
			for(Point n:tab_mem) {
				if(n.etreProche(x,y,FigureColoree.TAILLE_CARRE_SELECTION)){
					t=true;
				}
			}
		}
		return t;
	}
	
}