package modele;

import java.io.Serializable;

/*
 * Classe Point
 * 
 * @author millarde1u
 * @author schwarz16u
 */
@SuppressWarnings("serial")
public class Point implements Serializable {

	/*
	 * Abscisse du point
	 */
	private int x;
	
	/*
	 * Ordonnee du point
	 */
	private int y;
	
	/*
	 * Constructeur de la classe Point
	 * 
	 * @param nx
	 * 		Abscisse du point
	 * @param ny
	 * 		Ordonnee du point
	 */
	public Point(int nx, int ny) {
		this.x=nx;
		this.y=ny;
	}

	/*
	 * Constructeur de la classe Point a partir d'un point deja existant
	 * 
	 * @param p
	 * 		Point deja existant a partir duquel on construit un point
	 */
	public Point(Point p) {
		this.x=p.rendreX();
		this.y=p.rendreY();
	}

	/*
	 * Methode distance qui retourne la distance entre deux points
	 * 
	 * @return la distance entre deux points
	 * 
	 * @param p
	 * 		Deuxieme point avec lequel on calucle la distance
	 */
	public double distance(Point p) {
		double sommeX, sommeY, distance;
        if (p != null){
            sommeX = this.x - p.rendreX();
            sommeY = this.y - p.rendreY();
            if (sommeX < 0){
                sommeX = sommeX * -1;
            }
            if (sommeY < 0){
                sommeY = sommeY * -1;
            }
            distance = sommeX + sommeY;
        }
        else {
            distance = -1;
        }
        return distance;
	}
	
	/*
	 * Methode rendreX qui retourne l'abscisse du point
	 * 
	 * @return l'abscisse du point
	 */
	public int rendreX() {
		return this.x;
	}
	
	/*
	 * Methode rendreY qui retourne l'ordonnee du point
	 * 
	 * @return l'ordonnee du point
	 */
	public int rendreY() {
		return this.y;
	}
	
	/*
	 * Methode incrementerX qui permet d'incrementer l'abscisse du point
	 * 
	 * @param vx
	 * 		Increment a appliquer sur l'abscisse du poit
	 */
	public void incrementerX(int vx) {
		this.x= this.x + vx;
	}
	
	/*
	 * Methode incrementerY qui permet d'incrementer l'ordonnee du point
	 * 
	 * @param vy
	 * 		Increment a appliquer sur l'ordonnee du point
	 */
	public void incrementerY(int vy) {
		this.y=this.y +vy;
	}
	
	/*
	 * Methode modifierX qui permet de modifier l'abscisse du point
	 * 
	 * @param mx
	 * 		Nouvelle abscisse du point
	 */
	public void modifierX(int mx) {
		this.x = mx;
	}
	
	/*
	 * Methode modifierY qui permet de modifier l'ordonnee du point
	 * 
	 * @param my
	 * 		Nouvelle ordonnee du point
	 */
	public void modifierY(int my) {
		this.y=my;
		
	}
	
	/*
	 * Methode translation qui permet la translation du point
	 * 
	 * @param tx
	 * 		Deplacement en abscisse
	 * @param ty
	 * 		Deplacement en ordonnee
	 */
	public void translation (int tx, int ty) {
		this.incrementerX(tx);
		this.incrementerY(ty);
	}
	
	/*
	 * Methode etreProche qui retourne true si deux points sont dans une distance donnee
	 * 
	 * @return true si les points sont dans la distance donnee
	 * 
	 * @param px
	 * 		Abscisse du deuxieme point
	 * @param y
	 * 		Ordonnee du deuxieme point
	 * @param portee
	 * 		Distance donnee
	 */
	public boolean etreProche(int px, int py, int portee){
		Point p = new Point(px, py);
		double dist = this.distance(p);

		if (dist > portee){
			return false;

		}
		else {
			return true;
		}
	}
}