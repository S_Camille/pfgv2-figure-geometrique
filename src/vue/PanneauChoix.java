package vue;

import controleur.ControleClavier;
import modele.*;
import modele.Rectangle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

/*
 * Classe PanneauChoix permet d'initialiser les choix proposes dans la fenetre
 * 
 * @author millarde1u
 * @author schwarz16u
 */
@SuppressWarnings("serial")
public class PanneauChoix extends JPanel implements Serializable{

	/*
	 * Fenetre dans laquelle on va dessiner
	 */
	private DessinFigures dessin;
	/*
	 * Figure sur laquelle on travaille
	 */
	private FigureColoree fc;
	/*
	 * Indice de figure sur laquelle on travaille
	 */
	private int figure;
	/*
	 * Couleur de la figure
	 */
	private Color coul;
	/*
	 * Bouton qui permet le lancement de la palette de couleurs
	 */
	private JButton choixCoul;
	/*
	 * Booleen permettant de savoir si l'on est dans une creation de figure
	 */
	private boolean creationFigure;
	/*
	 * Menu deroulant permettant de creer les figures
	 */
	private JComboBox c;

	/*
	 * Constructeur de la classe PanneauChoix
	 * 
	 * @param d
	 * 		Fenetre dans laquelle on va dessiner
	 */
	public PanneauChoix(DessinFigures d){
		this.dessin = d;
		coul=Color.BLACK;
		creationFigure = true;
		JPanel ch = new JPanel();
		JPanel fcl = new JPanel();
		c = new JComboBox(new String[] {"Triangle","Quadrilatere", "Rectangle", "Carre", "Losange", "Cercle"});
		c.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				figure = ((JComboBox) (e.getSource())).getSelectedIndex();
				fc = creeFigure(figure + 1);
				dessin.construit(fc, coul);
			}
		});
		choixCoul = new JButton("");
		choixCoul.setBackground(coul);
		choixCoul.setOpaque(true);
		choixCoul.setBorderPainted(false);
		choixCoul.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Color c=JColorChooser.showDialog(null, "Choix couleur",coul);
				if (c!= null) {
					coul =c;
				}
				choixCoul.setBackground(coul);
				dessin.changementCouleur(coul);
			}
		});
		ButtonGroup group=new ButtonGroup();
		JRadioButton nouvFig=new JRadioButton("Nouvelle figure");
		nouvFig.setSelected(true);
		nouvFig.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				c.setVisible(true);
				choixCoul.setVisible(true);
				nouvFig.setDisplayedMnemonicIndex(0);
				dessin.desactiveManipulationsSouris();
				dessin.supprimeAuditeurs();
				fc=creeFigure(0);
				dessin.construit(fc,coul);
				creationFigure = true;
			}

		});
		JRadioButton mainLeve=new JRadioButton("Trace a main levee");
		mainLeve.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				c.setVisible(false);
				choixCoul.setVisible(true);
				dessin.supprimeAuditeurs();
				dessin.trace(coul);
				creationFigure = false;
			}

		});
		JRadioButton manipulation=new JRadioButton("Manipulations");
		manipulation.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				c.setVisible(false);
				choixCoul.setVisible(false);
				dessin.supprimeAuditeurs();
				dessin.activeManipulationsSouris();
				creationFigure = false;
			}

		});

		JButton ef = new JButton("Effacer");
		ef.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dessin.effacer();
			}
		});

		ControleClavier cc = new ControleClavier(dessin, this);
		this.addKeyListener(cc);
		this.setFocusable(true);
		this.requestFocusInWindow(true);
		group.add(nouvFig);
		group.add(mainLeve);
		group.add(manipulation);
		nouvFig.setFocusable(false);
		mainLeve.setFocusable(false);
		manipulation.setFocusable(false);
		this.setLayout(new BorderLayout());
		ch.add(nouvFig);
		ch.add(mainLeve);
		ch.add(manipulation);
		ch.add(ef);
		ef.setFocusable(false);
		fcl.add(c);
		c.setFocusable(false);
		fcl.add(choixCoul);
		choixCoul.setFocusable(false);
		this.add(ch, BorderLayout.NORTH);
		this.add(fcl, BorderLayout.SOUTH);
		fc = this.creeFigure(0);
		dessin.construit(fc,coul);
	}

	/*
	 * Methode creeFigure qui retourne la figure que l'on va dessiner
	 * 
	 * @return la figure que l'on va dessiner
	 * 
	 * @param nbFigure
	 * 		Indice de figure sur laquelle on travaille
	 */
	private FigureColoree creeFigure(int nbFigure){
		FigureColoree fc;
		switch (nbFigure){
			case 1 :
				fc = new Triangle(coul);
				break;
			case 2 :
				fc = new Quadrilatere(coul);
				break;
			case 3 :
				fc = new Rectangle(coul);
				break;
			case 4 :
				fc = new Carre(coul);
				break;
			case 5 :
				fc = new Losange(coul);
				break;
			case 6 :
				fc = new Cercle(coul);
				break;
			default :
				fc = new Triangle(coul);
				break;
		}
		return fc;
	}

	/*
	 * Methode renouvellement qui permet de recreer une figure du meme type qu'actuellement
	 */
	public void renouvellement(){
		dessin.construit(this.creeFigure(figure),coul);
	}

	@Override
	/*
	 * Methode paintComponent qui rafraichit tous les elements
	 * 
	 * @param g
	 * 		Environnement graphique du dessin
	 */
	public void paintComponent(Graphics g){
		super.paintComponent(g);
	}

	/*
	 * Methode changementCouleur qui permet de changer la couleur de la figure
	 *
	 * @param c
	 * 		Couleur de la figure
	 */
	public void changementCouleur(Color c){
		if (c!=null){
			this.coul = c;
			choixCoul.setBackground(coul);
		}
	}
	
	/*
	 * Methode getColor qui retourne la couleur de la figure
	 * 
	 * @return la couleur de la figure
	 *
	 */
	public Color getColor(){
		return this.coul;
	}

	/*
	 * Methode getCreationFigure qui retourne true si l'on est dans une creation de figure
	 * 
	 * @return true si l'on est dans une creation de figure
	 */
	public boolean getCreationFigure(){
		return this.creationFigure;
	}

	/*
	 * Methode setAffichageFigure qui permet de changer la figure affichee dans le menu deroulant
	 * 
	 * @param i
	 * 		Indice de figure sur laquelle on veut travailler
	 */
	public void setAffichageFigure(int i){
		if (i<c.getItemCount())
			c.setSelectedIndex(i);
	}
}
