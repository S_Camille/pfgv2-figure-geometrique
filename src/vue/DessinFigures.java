package vue;
import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import controleur.FabricantFigures;
import controleur.ManipulateurFormes;
import controleur.TraceurForme;
import modele.FigureColoree;

/*
 * Classe DessinFigures qui grace a chaque instance permet de faire un dessin comportant plusieurs figures colorees visualisables a l'ecran, 
 * dont une seule est selectionnee
 * 
 * @author millarde1u
 * @author schwarz16u
 */
@SuppressWarnings("serial")
public class DessinFigures extends JPanel implements Serializable {
	
	/*
	 * Liste qui contient les figures que l'on souhaite visualiser
	 */
	public List<FigureColoree> lfg;
	/*
	 * Objet "listener" pour la creation de formes via la souris
	 */
	private FabricantFigures lfc;
	/*
	 * Objet "listener" pour les manipulations et les transformations via la souris
	 */
	private ManipulateurFormes mf;
	/*
	 * Objet "listener" pour le tracage des traits a main levee via la souris
	 */
	private TraceurForme tf;
	/*
	 * Indice de la figure actuellement selectionee (-1 si aucune figure n'est selectionnee)
	 */
	private int sel;
	/*
	 * Liste qui contient les figures que l'on a annule
	 */
	private List<FigureColoree> undo;
	/*
	 * Booleen qui permet de dire si toute la liste lfg a ete supprimee
	 */
	private boolean suppression;
	/*
	 * Booleen qui permet de recuperer les elements supprimes s'il y a eu suppression
	 */
	private boolean suppressionSuite;
	
	/*
	 * Constructeur vide de la classe DessinFigures
	 */
	public DessinFigures(){
		lfg = new ArrayList<FigureColoree>();
		sel = -1;
		this.undo = new ArrayList<FigureColoree>();
	}

	/*
	 * Constructeur de la classe DessinFigures avec une liste de figures en parametre
	 * 
	 *@param lfg_tmp
	 *		Liste qui contient les figures que l'on souhaite visualiser
	 */
	public DessinFigures(List<FigureColoree> lfg_tmp){
		lfg = lfg_tmp;
		sel = -1;
		this.undo = new ArrayList<FigureColoree>();
		suppression = false;
		suppressionSuite = false;
	}

	/*
	 * Methode paintComponent qui redessine toutes les figures du dessin
	 * 
	 * @param g
	 *		Environnement graphique du dessin
	 */
	@Override
	public void paintComponent(Graphics g){
		this.setBackground(Color.WHITE);
		super.paintComponent(g);
		for(FigureColoree n:this.lfg){
			n.affiche(g);
		}
	}
	/*
	 * Methode ajoute qui permet d'ajouter une nouvelle figure au dessin
	 *
	 * @param fc
	 * 		Figure que l'on souhaite ajouter au dessin
	 */
	public void ajoute(FigureColoree fc){
		this.lfg.add(fc);
		this.undo.add(fc);
	}

	/*
	 * Methode construit qui peremet d'initier le mecanisme evenementiel de la fabrication des figures 
	 * a l'aide de la souris (ajout du listener)
	 *
	 * @param fc
	 * 		Figure a construire point par point avec la souris
	 *  @param c
	 *  	Couleur de la figure que l'on souhaite construire
	 */
	public void construit(FigureColoree fc,Color c){
		suppression = false;
		suppressionSuite = false;
		this.supprimeAuditeurs();
		this.lfc = new FabricantFigures(fc,c);
		this.addMouseListener(this.lfc);
	}

	/*
	 * Methode supprimeAuditeurs qui permet de supprimer tous les auditeurs utilises dans cette classe
	 */
	public void supprimeAuditeurs(){
		this.removeMouseListener(this.lfc);
		this.removeMouseListener(mf);
		this.removeMouseMotionListener(mf);
		this.removeMouseMotionListener(tf);
	}

	/*
	 * Methode activeManipulationsSouris qui active les manipulations des formes a l'aide de la souris
	 */
	public void activeManipulationsSouris(){
		mf = new ManipulateurFormes(this.lfg);
		this.addMouseListener(mf);
		this.addMouseMotionListener(mf);
	}
	
	/*
	 * Methode desactiveManipulationsSouris qui desactive les manipulations des formes a l'aide de la souris
	 */
	public void desactiveManipulationsSouris(){
		this.removeMouseMotionListener(mf);
	}
	
	/*
	 * Methode nbFigures qui retourne le nombre de figures apparaissant dans ce dessin
	 * 
	 * @return le nombre de figures apparaissant dans ce dessin
	 */
	public int nbFigures(){
		return this.lfg.size();
	}

	/*
	 * Methode figureSelection qui retourne la figure actuellement selectionnee
	 * 
	 * @return la figure actuellement selectionnee
	 */
	public FigureColoree figureSelection(){
		if (sel != -1){
			return this.lfg.get(sel);
		}
		return null;
	}

	/*
	 * Methode selectionProchaineFigure qui permet de selectionner la prochaine figure dans le tableau des figures
	 *
	 */
	public void selectionProchaineFigure(){
		if (sel<this.lfg.size()-2){
			sel++;
		}
		else{
			sel = 0;
		}
	}

	/*
	 * Methode changementCouleur qui permet de changer la couleur de la figure et/ou celle du trait 
	 * en cours de construction
	 * 
	 * @param c
	 * 		Couleur que l'on souhaite mettre a la place de celle deja mise
	 */
	public void changementCouleur(Color c){
		try {
			this.lfc.changementCouleur(c);
			this.tf.changerCouleur(c);
			this.repaint();
		}
		catch (NullPointerException e){

		}
	}

	/*
	 * Methode trace qui permet d'initier le mecaisme evenementiel de trace quelconque a l'aide 
	 * de la souis (definition de la couleur du trace et ajout des listener)
	 * 
	 * @param c
	 * 		Couleur du trait
	 */
	public void trace(Color c){
		Graphics g = this.getGraphics();
		tf=new TraceurForme(g, c);
		this.addMouseListener(tf);
		this.addMouseMotionListener(tf);
	}

	/*
	 * Methode ouvrir qui permet de recuperer une classe DessinFigures a l'aide d'un fichier donne en parametre
	 * 
	 * @param file
	 * 		Fichier que l'on souhaite ouvrir, contenant une classe DessinFigures
	 */
	public static DessinFigures ouvrir(File file){
		try {
			ObjectInputStream f = new ObjectInputStream(new FileInputStream(file));
			DessinFigures df = (DessinFigures)f.readObject();
			return df;
		} 
		catch (Exception e) {
			return null;
		}
	}

	/*
	 * Methode sauvegarder permettant de sauvegarder la classe dans un fichier passe en parametre
	 * 
	 * @param file
	 *		Fichier ou l'on souhaite sauvegarder la classe
	 */
	public void sauvegarder(File file){
		try{
			ObjectOutputStream f = new ObjectOutputStream(new FileOutputStream(file));
			f.writeObject(this);
			f.close();
		}
		catch(Exception e){
			System.out.println("Erreur Inconnue");
			e.printStackTrace();
		}
	}

	/*
	 * Methode effacer qui permet d'effacer la liste lfg
	 */
	public void effacer(){
		suppression = true;
		this.undo = this.lfg;
		this.lfg = new ArrayList<FigureColoree>();
		this.repaint();
	}
	
	/*
	 * Methode undo qui permet d'annuler la derniere figure
	 */
	public void undo(){
		if(this.undo == null) {
			undo = new ArrayList<FigureColoree>();
		}
		int i = this.lfg.size()-1;
		if(i != -1) {
			this.undo.add(this.lfg.get(i));
			this.lfg.remove(this.lfg.get(i));
		}
		if (suppression) {
			this.lfg = this.undo;
			this.undo = new ArrayList<FigureColoree>();
			suppression = false;
			suppressionSuite = true;
		}
		this.repaint();
	}
	
	/*
	 * Methode redo qui permet d'annuler l'annulation
	 */
	public void redo(){
		if(undo.size() != 0){
			if (suppressionSuite){
				this.undo = this.lfg;
				suppressionSuite = false;
			}
			else {
				int i = undo.size() - 1;
				this.lfg.add(this.undo.get(i));
				this.undo.remove(i);
			}
		}
		this.repaint();
	}

	/*
	 * Methode viderUndo qui permet de vider la memoire de liste undo
	 *
	 */
	public void viderUndo(){
		this.undo = new ArrayList<FigureColoree>();
	}

	/*
	 * Methode effacer qui permet d'effacer la figure actuellent selectionnee
	 * 
	 * @param sel
	 * 		Indice de la figure actuellement selectionee (-1 si aucune figure n'est selectionnee)
	 */
    public void effacer(int sel) {
        this.lfg.remove(sel);
        this.repaint();
	}
	/*
	 * Methode changementCouleur qui permet de changer la couleur de la figure selectionnee
	 * 
	 * @param sel
	 * 		Indice de la figure actuellement selectionee (-1 si aucune figure n'est selectionnee)
	 */
    public void changementCouleur(int sel) {
	    Color c = JColorChooser.showDialog(null, "Choix de la couleur",this.lfg.get(sel).getColor());
	    if (c!= null){
            this.lfg.get(sel).changeCouleur(c);
	    }
	    this.repaint();
    }
    
	/*
	 * Methode getNbElementUndo qui permet de retourner le nombre d'elements de la liste undo
	 */
    public int getNbElementUndo(){
		return this.undo.size();
	}

}