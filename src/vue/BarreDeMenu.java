package vue;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.*;

import main.Fenetre;
/*
 * Classe BarreDeMenu qui permet la creation d'une barre de menu dans la fenetre du projet
 *
 * @author millarde1u
 * @author schwarz16u
 */
public class BarreDeMenu {

	/*
	 * Barre de menu
	 */
	private JMenuBar menuBar;
	/*
	 * Element annuler
	 */
	private JMenuItem undo;
	/*
	 * Element retablir
	 */
	private JMenuItem redo;

	/*
	 * Constructeur de la classe BarreDeMenu
	 * 
	 * @param df
	 * 		Fenetre dans laquelle on va dessiner
	 */
	public BarreDeMenu(DessinFigures df){
		menuBar = new JMenuBar();
		JMenu fichier = new JMenu("Fichier");
		fichier.getAccessibleContext().setAccessibleDescription("Test des Jmenus");
		JMenuItem nouv = new JMenuItem("Nouvelle fenetre");
		nouv.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				new Fenetre("Figure Geometrique (Secondaire)",800,600, null, JFrame.DISPOSE_ON_CLOSE);
			}

		});
		nouv.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_MASK));
		fichier.add(nouv);
		JMenuItem ouvrir = new JMenuItem("Ouvrir ...");
		ouvrir.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser btdo=new JFileChooser(new File("."));
				btdo.showOpenDialog(df);
				DessinFigures df = DessinFigures.ouvrir(btdo.getSelectedFile());
				if (df != null)
					new Fenetre("Figure Geometrique (Secondaire)",800, 600,df,JFrame.DISPOSE_ON_CLOSE );

			}
		});
		ouvrir.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_MASK));
		fichier.add(ouvrir);
		fichier.addSeparator();
		JMenuItem sauv = new JMenuItem("Sauvegarder sous ...");
		sauv.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser btdo=new JFileChooser(new File("."));
				btdo.showSaveDialog(df);
				df.sauvegarder(btdo.getSelectedFile());
			}

		});
		sauv.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_MASK));
		fichier.add(sauv);
		menuBar.add(fichier);

		JMenu editer = new JMenu("Editer");
		undo = new JMenuItem("Annuler la derniere figure");
		undo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				df.undo();
				redo.setEnabled(true);
			}
		});
		undo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_MASK));
		editer.add(undo);
		redo = new JMenuItem("Retablir la derniere figure effacee");
		redo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				df.redo();
				if(df.getNbElementUndo() == 0){
					redo.setEnabled(false);
				}
			}
		});
		redo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Y, KeyEvent.CTRL_MASK));
		redo.setEnabled(false);
		editer.add(redo);
		menuBar.add(editer);


		JMenu dessin = new JMenu("Dessin");
		JMenuItem effacer = new JMenuItem("Effacer la page");
		effacer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				df.effacer();
			}
		});
		effacer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_MASK));
		dessin.add(effacer);
		menuBar.add(dessin);
	}
	
	/*
	 * Methode getMenuBar qui retourne la barre de menu
	 * 
	 * @return la barre de menu
	 */
	public JMenuBar getMenuBar(){
		return menuBar;
	}
}