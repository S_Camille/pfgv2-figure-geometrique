package controleur;

import java.awt.event.*;
import java.io.Serializable;
import java.util.List;

import modele.FigureColoree;
import vue.DessinFigures;

import javax.swing.*;
import javax.swing.border.BevelBorder;

/*
 * Classe ManipulateurFormes qui permet de manipuler/modifier les formes
 * 
 * @author millarde1u
 * @author schwarz16u
 */
@SuppressWarnings("serial")
public class ManipulateurFormes implements MouseListener, MouseMotionListener, Serializable {

	/*
	 * Derniere position en abscisse
	 */
	private int last_x;
	/*
	 * Derniere position en ordonnee
	 */
	private int last_y;
	/*
	 * Indice de la figure selectionnee
	 */
	private int sel;
	/*
	 * Booleen qui permet d'indiquer si on transforme la figure
	 */
	private boolean trans;
	/*
	 * Liste qui contient les figures que l'on souhaite visualiser
	 */
	private List<FigureColoree> lfg;
	/*
	 * Indice du point
	 */
	private int ind_pt;
	/*
	 * Menu popup
	 */
	JPopupMenu popup;
	
	/*
	 * Constructeur de la classe ManipulateurFormes
	 * 
	 * @param  tmp_lfg
	 * 		Liste qui contient les figures que l'on souhaite visualiser
	 */
	public ManipulateurFormes(List<FigureColoree> tmp_lfg) {
		sel = -1;
		trans = false;
		this.lfg = tmp_lfg;
	}

	/*
	 * Methode mouseClicked non utilisee
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	@Override
	public void mouseClicked(MouseEvent e) {}

	/*
	 * Methode mousePressed qui permet de gerer tous les evenements lies au clic de la souris
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		last_x = e.getX();
		last_y = e.getY();
		if(SwingUtilities.isRightMouseButton(e)){
			if (sel != -1){
				DessinFigures df = ((DessinFigures)e.getSource());
				popup = new JPopupMenu();
				JMenuItem  effacer = new JMenuItem("Effacer la figure");
				effacer.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
 						df.effacer(sel);
					}
				});
				popup.add(effacer);
				JMenuItem  couleur = new JMenuItem("Changer la couleur ...");
				couleur.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						df.changementCouleur(sel);
					}
				});
				popup.add(couleur);
				popup.setBorder(new BevelBorder(BevelBorder.RAISED));
				popup.show(df,e.getX(), e.getY());
			}
		}
	}

	/*
	 * Methode mouseReleased qui permet de gerer le relachement du clic de la souris
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		boolean pass = true;
		if (popup != null){
			if (popup.isVisible()){
				pass = false;
			}
		}
		if(pass) {
			trans = false;
			ind_pt = -1;
		}
	}

	/*
	 * Methode mouseEntered non utilisee
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	@Override
	public void mouseEntered(MouseEvent e) {}

	/*
	 * Methode mouseExited non utilisee
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	@Override
	public void mouseExited(MouseEvent e) {}

	/*
	 * Methode mouseMoved qui permet de gerer les evenements lorsque la souris est deplacee en maintenant le clic
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		if (sel != -1){
			FigureColoree fc = this.lfg.get(sel);
			DessinFigures df =((DessinFigures)e.getSource());
			if (fc.carreDeSelection(last_x, last_y) != -1 || trans){
				trans = true;
				if (fc.carreDeSelection(last_x, last_y) != -1){
					ind_pt = fc.carreDeSelection(last_x, last_y);
				}
				fc.transformation(e.getX(), e.getY(), ind_pt);
				df.repaint();
			}
			else{
			fc.translation(e.getX()-last_x, e.getY()-last_y);
			((DessinFigures)e.getSource()).repaint();
			last_x = e.getX();
			last_y = e.getY();
			}
		}
	}

	/*
	 * Methode mouseMoved qui permet de gerer le deplacement de la souris
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		boolean pass = true,mod=false;
		DessinFigures df = ((DessinFigures)e.getSource());
		last_x = e.getX();
		last_y = e.getY();
		if (popup != null) {
			if (popup.isVisible()) {
				pass = false;
			}
		}
		if(pass){
			for (FigureColoree n : lfg) {
				if (n.estDedans(e.getX(), e.getY())) {
					n.selectionne();
					df.repaint();
					sel = lfg.indexOf(n);
					mod = true;
				} else {
					n.deSelectionne();
					if (!mod) {
						sel = -1;
					}
					df.repaint();
				}
			}
		}
	}

}