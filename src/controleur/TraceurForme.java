package controleur;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.Serializable;

import javax.swing.SwingUtilities;

import modele.FigureColoree;
import modele.Point;
import modele.Trait;
import vue.DessinFigures;

/*
 * Classe TraceurForme qui permet d'effectuer le trace a main levee
 * 
 * @author millarde1u
 * @author schwarz16u
 */
@SuppressWarnings("serial")
public class TraceurForme implements MouseListener, MouseMotionListener,Serializable {
	
	/*
	 * Contexte graphique
	 */
	private Graphics g;
	/*
	 * Abscisse d'un clic de souris
	 */
	private int last_x;
	
	/*
	 * Ordonnee d'un clic de souris
	 */
	private int last_y;
	/*
	 * Couleur du trace
	 */
	private Color coul;
	
	/*
	 * Constructeur de la classe TraceurForme
	 * 
	 * @param gr
	 * 		Contexte graphique
	 * @param c
	 * 		Couleur du trace
	 */
	public TraceurForme(Graphics gr, Color c){
		this.g = gr;
		this.coul = c;
	}


	/*
	 * Methode mouseDrgged qui effectue le trace a main levee
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)){
			Point[] tab_p = {new Point(last_x,last_y),new Point(e.getX(),e.getY())};
			FigureColoree fc = new Trait(coul);
			fc.modifierPoints(tab_p);
			DessinFigures df = ((DessinFigures)e.getSource());
			df.ajoute(fc);
			df.repaint();
			last_x=e.getX();
			last_y=e.getY();
			
		}
	}

	/*
	 * Methode mouseMoved non utilisee
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	@Override
	public void mouseMoved(MouseEvent e) {}

	/*
	 * Methode mouseClicked non utilisee
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	@Override
	public void mouseClicked(MouseEvent e) {}

	/*
	 * Methode mousePressed qui debute le trace de main levee
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)){
			last_x = e.getX();
			last_y = e.getY();
		}
	
	}

	/*
	 * Methode mouseReleased non utilisee
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	@Override
	public void mouseReleased(MouseEvent e) {}


	/*
	 * Methode mouseEntered non utilisee
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	@Override
	public void mouseEntered(MouseEvent e) {}

	/*
	 * Methode mouseExited non utilisee
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	@Override
	public void mouseExited(MouseEvent e) {}

	/*
	 * Methode changerCouleur qui permet de changer la couleur de la figure
	 * 
	 * @param c
	 * 		Nouvelle couleur a attribuer a la figure
	 */
	public void changerCouleur(Color c){
		if (c != null){
			this.coul = c;
		}
	}
}