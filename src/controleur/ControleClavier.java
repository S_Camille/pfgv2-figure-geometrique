package controleur;

import modele.*;
import modele.Rectangle;
import vue.DessinFigures;
import vue.PanneauChoix;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/*
 * Classe ControleClavier qui permet de controler les evenements lies au clavier
 */
public class ControleClavier extends KeyAdapter{

	/*
	 * Fenetre dans laquelle on va dessiner
	 */
    private DessinFigures dessin;
    /*
     * Couleur de la figure
     */
    private Color coul;
    /*
     * Panneau de choix dans lequel on va travailler
     */
    private PanneauChoix pc;

    /*
     * Constructeur de la classe ControleClavier
     * 
     * @param df
     * 		Fenetre dans laquelle on va dessiner
     * @param pc_tmp
     * 		Tableau de panneau de choix dans lequel on va travailler
     */
    public ControleClavier(DessinFigures df,PanneauChoix pc_tmp){
        dessin = df;
        pc= pc_tmp;
    }
    
	/*
	 * Methode keyTyped non utilisee
	 * 
	 * @param e
	 * 		Evenement du clavier
	 */
    @Override
    public void keyTyped(KeyEvent e) {

    }

    /*
	 * Methode keyPressed qui permet de gerer quand une touche du clavier est appuyee
	 * 
	 * @param e
	 * 		Evenement du clavier
	 */
    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyChar()) {
            case 'c':
                Color c = JColorChooser.showDialog(dessin, "Choix de la couleur", pc.getColor());
                if (c != null) {
                    dessin.changementCouleur(c);
                    pc.changementCouleur(c);

                }
                break;
            case 'l':
                if (pc.getCreationFigure()) {
                    dessin.construit(new Losange(coul), coul);
                    pc.setAffichageFigure(4);
                }
                break;
            case 'r':
                if (pc.getCreationFigure()) {
                    dessin.construit(new Rectangle(coul), coul);
                    pc.setAffichageFigure(2);
                }
                break;
            case 't':
                if (pc.getCreationFigure()) {
                    dessin.construit(new Triangle(coul), coul);
                    pc.setAffichageFigure(0);
                }
                break;
            case 'a':
                if (pc.getCreationFigure()){
                    dessin.construit(new Carre(coul),coul);
                    pc.setAffichageFigure(3);
                }
                break;
            case 'q':
                if (pc.getCreationFigure()) {
                    dessin.construit(new Quadrilatere(coul), coul);
                    pc.setAffichageFigure(1);
                }
                break;
            case 'e':
                if (pc.getCreationFigure()) {
                    dessin.construit(new Cercle(coul), coul);
                    pc.setAffichageFigure(5);
                }
                break;
            default :
                break;
        }
    }

    /*
	 * Methode keyReleased non utilisee
	 * 
	 * @param e
	 * 		Evenement du clavier
	 */
    @Override
    public void keyReleased(KeyEvent e) {

    }
    
    /*
	 * Methode changementCouleur qui permet de changer la couleur de la figure
	 * 
	 * @param c
	 * 		Nouvelle couleur a attribuer a la figure
	 */
    public void changementCouleur(Color c){
        if (c!=null){
            coul = c;
        }
    }
}