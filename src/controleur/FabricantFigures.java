package controleur;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;

import modele.FigureColoree;
import modele.Point;
import vue.DessinFigures;

/*
 * Classe FabricantFigures qui implemente la creation de figures geometriques via des clics de souris
 * 
 * @author millarde1u
 * @author schwarz16u
 */
@SuppressWarnings("serial")
public class FabricantFigures implements MouseListener, Serializable{

	/*
	 * Entier qui accumule le nombre de clics de souris
	 */
	private int nb_points_cliques;
	/*
	 * Figure en cours de fabrication
	 */
	private FigureColoree figure_en_cours_de_fabrication;
	/*
	 * Tableau contenant des points crees a partir de clics de souris
	 */
	private Point[] points_cliques;
	/*
	 * Couleur de la figure
	 */
	Color c;
	
	/*
	 * Constructeur de la classe FabricantFigures
	 * 
	 * @param fc
	 * 		Figure coloree
	 * @param c_tmp
	 * 		Couleur de la figure
	 */
	public FabricantFigures(FigureColoree fc, Color c_tmp) {
		figure_en_cours_de_fabrication = fc;
		nb_points_cliques = 0;
		points_cliques=new Point[fc.nbClics()];
		this.c = c_tmp;
	}
	
	/*
	 * Methode mouseEntered non utilisee
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	public void mouseEntered(MouseEvent me) {}
	
	/*
	 * Methode mouseExited non utilisee
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	public void mouseExited(MouseEvent me) {}
	
	/*
	 * Methode mouseReleased non utilisee
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	public void mouseReleased(MouseEvent me) {}

	/*
	 * Methode mouseClicked non utilisee
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	public void mouseClicked(MouseEvent me) {}
	
	/*
	 * Methode mousePassed qui permet d'implementer la creation d'une figure geometrique 
	 * via des clics de souris
	 * 
	 * @param me
	 * 		Evenement de la souris
	 */
	public void mousePressed(MouseEvent me) {
		DessinFigures df = ((DessinFigures)me.getSource());
		df.viderUndo();
		Graphics g = df.getGraphics();
		if(nb_points_cliques != (figure_en_cours_de_fabrication.nbClics()-1)){
			g.fillRect(me.getX()-(FigureColoree.TAILLE_CARRE_SELECTION/2),me.getY()-(FigureColoree.TAILLE_CARRE_SELECTION/2),FigureColoree.TAILLE_CARRE_SELECTION,FigureColoree.TAILLE_CARRE_SELECTION);
			points_cliques[nb_points_cliques] = new Point(me.getX(),me.getY());
			nb_points_cliques++;
		}
		else{
			points_cliques[nb_points_cliques] = new Point(me.getX(),me.getY());
			figure_en_cours_de_fabrication.modifierPoints(points_cliques);
			df.ajoute(figure_en_cours_de_fabrication);
			df.repaint();
			figure_en_cours_de_fabrication = figure_en_cours_de_fabrication.clone();
			nb_points_cliques = 0;
			points_cliques=new Point[figure_en_cours_de_fabrication.nbClics()];

		}
	}
	
	/*
	 * Methode changementCouleur qui peremet le changement de couleur pour la figure en cours 
	 * de fabrication
	 * 
	 * @param c
	 * 		Couleur de la figure
	 */
	public void changementCouleur(Color c){
		figure_en_cours_de_fabrication.changeCouleur(c);
	}
	
}