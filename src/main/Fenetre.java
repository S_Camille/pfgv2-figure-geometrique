package main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JPanel;

import vue.*;

@SuppressWarnings("serial")
/*
 * Classe Fenetre qui definit l'interface utilisateur et la methode main
 * 
 * @author millarde1u
 * @author schwarz16u
 */
public class Fenetre extends JFrame {
	
	/*
	 * Contenu de la fenetre principale (JPanel)
	 */
	private DessinFigures dessin;

	/*
	 *  Constructeur de la classe Fenetre
	 *  
	 *  @param s
	 *  	Nom de la fenetre
	 *  @param w
	 *  	Largeur de la fenetre
	 *  @param h
	 *  	Hauteur de la fenetre
	 *  @param df
	 *  	Contenu de la fenetre principale (JPanel)
	 */
	public Fenetre(String s, int w, int h, DessinFigures df, int jframe){
		super(s);
		this.setDefaultCloseOperation(jframe);
		System.setProperty("apple.laf.useScreenMenuBar", "true");
		JPanel jp=new JPanel();
		if(df == null){
			dessin = new DessinFigures();
		}
		else{
			dessin = df;
		}
		PanneauChoix pc = new PanneauChoix(dessin);
		BarreDeMenu barreDeMenu = new BarreDeMenu(dessin);
		jp.setLayout(new BorderLayout());
		jp.add(pc, BorderLayout.NORTH);
		jp.add(dessin,BorderLayout.CENTER);
		jp.setPreferredSize(new Dimension(w,h));
		pc.requestFocusInWindow();
		this.setJMenuBar(barreDeMenu.getMenuBar());
		this.setContentPane(jp);
		this.pack();
		this.setVisible(true);
	}
	
	/*
	 * Methode main qui correspond au programme principal
	 * 
	 * @param args
	 * 		Tableau d'arguments
	 */
	public static void main(String[] args){
		new Fenetre("Figure Geometrique (Princpale)",800,600,null,JFrame.EXIT_ON_CLOSE );
	}
	
}
